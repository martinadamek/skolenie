package com.example.projekt1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class SimpleActivity extends Activity {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.simple);

        Intent resultData = new Intent();
        resultData.putExtra("nieco", "Ahoj skolenie");
        setResult(Activity.RESULT_OK, resultData);
    }

}