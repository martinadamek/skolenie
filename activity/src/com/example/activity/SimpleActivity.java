package com.example.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class SimpleActivity extends Activity {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.simple);

        final EditText editText = (EditText) findViewById(R.id.text);
        findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String text = editText.getText().toString().trim();
                if (text.equals("")) {
                    setResult(Activity.RESULT_CANCELED);
                } else {
                    Intent data = new Intent();
                    data.putExtra("moj_text", text);
                    setResult(Activity.RESULT_OK, data);
                }
                finish();
            }
        });
    }

}