package com.example.service;

import android.app.IntentService;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class SimpleService extends IntentService {

    public SimpleService() {
        super("SimpleService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        for (int i = 0; i < 5; i++) {
            try {
                System.out.println("### Dlhy task...");
                Thread.sleep(1000);
                System.out.println("### ... hotovo");
            } catch (InterruptedException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        Intent broadcastIntent = new Intent("action_meno");
        broadcastIntent.putExtra("meno", "Praha");
        sendBroadcast(broadcastIntent);
    }

}
