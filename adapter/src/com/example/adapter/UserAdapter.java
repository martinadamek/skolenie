package com.example.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class UserAdapter extends BaseAdapter {

    private final LayoutInflater mInflater;
    private final List<User> mUsers;

    public UserAdapter(LayoutInflater inflater, List<User> users) {
        this.mInflater = inflater;
        this.mUsers = users;
    }

    @Override
    public int getCount() {
        return mUsers.size();
    }

    @Override
    public Object getItem(int i) {
        return mUsers.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        View view = convertView;
        if (view == null) {
            view = mInflater.inflate(R.layout.user_row, viewGroup, false);
            ViewHolder viewHolder = new ViewHolder(
                    (TextView) view.findViewById(R.id.name), (TextView) view.findViewById(R.id.age));
            view.setTag(viewHolder);
        }
        ViewHolder viewHolder = (ViewHolder) view.getTag();
        viewHolder.txtName.setText(mUsers.get(i).getName());
        viewHolder.txtAge.setText(Integer.toString(mUsers.get(i).getAge()));
        return view;
    }

    private class ViewHolder {
        TextView txtName;
        TextView txtAge;

        private ViewHolder(TextView txtName, TextView txtAge) {
            this.txtName = txtName;
            this.txtAge = txtAge;
        }
    }

}
