package com.example.adapter;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.Arrays;
import java.util.List;

public class MyActivity extends Activity {
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

//        ListAdapter adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, new String[] {
//                "Mercedes", "Audi", "BMW", "Porsche"
//        });

        List<User> users = Arrays.asList(
                new User("Martin", 36),
                new User("Peter", 34),
                new User("Michal", 23)
        );
        UserAdapter adapter = new UserAdapter(getLayoutInflater(), users);

        ListView listView = (ListView) findViewById(R.id.list);
        listView.setAdapter(adapter);
    }

}
