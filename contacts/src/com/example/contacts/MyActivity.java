package com.example.contacts;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;

public class MyActivity extends Activity {
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        Cursor cursor = getContentResolver().query(
                ContactsContract.Contacts.CONTENT_URI,
                new String[] {
                        ContactsContract.Contacts._ID,
                        ContactsContract.Contacts.DISPLAY_NAME
                },
                null, null, null
        );

        if (cursor.moveToFirst()) {
            do {
                System.out.println("### " +
                        cursor.getString(0) + ": " +
                        cursor.getString(1)
                );
            } while (cursor.moveToNext());
        }

        cursor.close();
    }
}
