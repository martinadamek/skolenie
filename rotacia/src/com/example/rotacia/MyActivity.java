package com.example.rotacia;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MyActivity extends Activity {

    private String mMeno;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText editText = (EditText) findViewById(R.id.text);
                mMeno = editText.getText().toString();
            }
        });

        if (savedInstanceState != null) {
            mMeno = savedInstanceState.getString("text");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
//        Toast.makeText(this, "Meno: " + mMeno, Toast.LENGTH_SHORT).show();
        System.out.println("### onResume " + mMeno);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("text", mMeno);
    }

}
