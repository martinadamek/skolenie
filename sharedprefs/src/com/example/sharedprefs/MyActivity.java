package com.example.sharedprefs;

import android.app.Activity;
import android.os.Bundle;

public class MyActivity extends Activity {
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        getSharedPreferences("MyPrefsFile", MODE_PRIVATE).edit()
                .putString("meno", "Martin")
                .commit();

        getSharedPreferences("MyPrefsFile", MODE_PRIVATE).getString("meno", null);

    }
}
