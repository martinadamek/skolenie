package com.example.sqlite;

import android.app.Activity;
import android.app.ListActivity;
import android.app.LoaderManager;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.widget.CursorAdapter;
import android.widget.SimpleCursorAdapter;
import com.commonsware.cwac.loaderex.SQLiteCursorLoader;

public class MyActivity extends ListActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    private CarHelper mCarHelper;
    private CursorAdapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mCarHelper = new CarHelper(this);

//        mAdapter = new SimpleCursorAdapter(this,
//                android.R.layout.simple_list_item_2, null,
//                new String[] { "manufacturer", "model" },
//                new int[] { android.R.id.text1, android.R.id.text2 }, 0);

        mAdapter = new CarAdapter(this);

        setListAdapter(mAdapter);
        getLoaderManager().initLoader(0, null, this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new SQLiteCursorLoader(this, mCarHelper, "select * from cars", null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        mAdapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {
        mAdapter.swapCursor(null);
    }

}
