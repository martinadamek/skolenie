package com.example.sqlite;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

public class CarAdapter extends CursorAdapter {

    private LayoutInflater mInflater;

    public CarAdapter(Context context) {
        super(context, null, false);
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        View view = mInflater.inflate(R.layout.car_row, viewGroup, false);
        ViewHolder holder = new ViewHolder(
                (TextView) view.findViewById(R.id.manufacturer),
                (TextView) view.findViewById(R.id.model),
                (TextView) view.findViewById(R.id.hp));
        view.setTag(holder);
        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ViewHolder holder = (ViewHolder) view.getTag();
        holder.txtManufacturer.setText(cursor.getString(1));
        holder.txtModel.setText(cursor.getString(2));
        holder.txtHp.setText(cursor.getString(3));
    }

    private static class ViewHolder {
        final TextView txtManufacturer;
        final TextView txtModel;
        final TextView txtHp;

        private ViewHolder(TextView txtManufacturer, TextView txtModel, TextView txtHp) {
            this.txtManufacturer = txtManufacturer;
            this.txtModel = txtModel;
            this.txtHp = txtHp;
        }
    }

}
