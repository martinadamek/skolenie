package com.example.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class CarHelper extends SQLiteOpenHelper {

    public CarHelper(Context context) {
        super(context, "cars.db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table cars (" +
                "_id integer primary key autoincrement," +
                "manufacturer text," +
                "model text," +
                "hp integer)");
        db.insert("cars", null, newCar("Porsche", "Cayenne S", 340));
        db.insert("cars", null, newCar("Audi", "A6", 180));
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists cars");
        onCreate(db);
    }

    private static ContentValues newCar(String manufacturer, String model, int hp) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("manufacturer", manufacturer);
        contentValues.put("model", model);
        contentValues.put("hp", hp);
        return contentValues;
    }
}
