package com.example.http;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.CharBuffer;
import java.util.Scanner;

public class MyActivity extends Activity {

    private static final String URL =
            "http://api.openweathermap.org/data/2.5/weather?q=Prague,cz&units=metric";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    String json = downloadUrl(URL);
                    parseJson(json);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute();
    }

    private String downloadUrl(String myurl) throws IOException {
        InputStream is = null;
        try {
            URL url = new URL(myurl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            is = conn.getInputStream();
            return new Scanner(is).useDelimiter("\\A").next();
        } finally {
            if (is != null) {
                is.close();
            }
        }
    }

    private void parseJson(String json) throws JSONException {
        JSONObject root = new JSONObject(json);
        JSONObject main = root.getJSONObject("main");
        System.out.println("### Teplota: " + main.getDouble("temp"));
        System.out.println("### Tlak: " + main.getDouble("pressure"));
        System.out.println("### Vlhkost: " + main.getDouble("humidity"));
    }

}
