package com.example.notification;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MyActivity extends Activity {
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Context context = MyActivity.this;

                PendingIntent pendingIntent = PendingIntent.getActivity(
                        context,
                        123,
                        new Intent(context, MyActivity.class),
                        PendingIntent.FLAG_UPDATE_CURRENT);

                Notification.Builder builder =
                        new Notification.Builder(MyActivity.this)
                                .setSmallIcon(R.drawable.ic_launcher)
                                .setContentTitle("Ahoj")
                                .setContentText("Toto je notifkacia")
                                .setContentIntent(pendingIntent)
                                .setAutoCancel(true);

                NotificationManager manager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                manager.notify(456, builder.build());
            }
        });
    }
}
