package com.example.settings;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;

public class MyActivity extends PreferenceActivity {
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
    }

    @Override
    protected void onDestroy() {

        SharedPreferences prefs =
                PreferenceManager.getDefaultSharedPreferences(this);
        System.out.println("### PREF " + prefs.getBoolean("pref_sync", false));

        super.onDestroy();
    }
}
